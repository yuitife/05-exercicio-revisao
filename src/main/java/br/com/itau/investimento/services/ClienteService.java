package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.repositories.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	ClienteService clienteService;
	
	public Iterable<Cliente> obterListaCliente() {
		return clienteRepository.findAll();
	}
	
	public Cliente obterClientePorId(int id) {
		Optional<Cliente> clienteQuery = clienteRepository.findById(id);
		if (clienteQuery.isPresent()) {
			return clienteQuery.get();
		}
		return null;
	}
	
	public boolean inserirCliente(Cliente clienteNovo) {
		clienteRepository.save(clienteNovo);
		return true;
	}
	
	public boolean alterarCliente(Cliente clienteAlterado) {
		Cliente clienteBase = clienteService.obterClientePorId(clienteAlterado.getId());
		if (clienteBase.getCpf() != clienteAlterado.getCpf() &&
		   (! clienteAlterado.getCpf().isEmpty())) {
			clienteBase.setCpf(clienteAlterado.getCpf());
		}
		if (clienteBase.getEmail() != clienteAlterado.getEmail() &&
				(! clienteAlterado.getEmail().isEmpty())) 		{
			clienteBase.setEmail(clienteAlterado.getEmail());
		}
		if (clienteBase.getNome() != clienteAlterado.getNome() && 
				(! clienteAlterado.getNome().isEmpty())) {
			clienteBase.setNome(clienteAlterado.getNome());
		}
		clienteRepository.save(clienteBase);
		return true;
	}
}
