package br.com.itau.investimento.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.investimento.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{

}
