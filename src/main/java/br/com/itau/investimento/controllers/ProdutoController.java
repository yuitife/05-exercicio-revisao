package br.com.itau.investimento.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.services.CatalogoService;

@RestController
@RequestMapping("/produtoinvestimento")
public class ProdutoController {
	@Autowired
	CatalogoService catalogoService;
	
	@GetMapping
	public Iterable<Produto> listarProduto() {		
		return catalogoService.obterProdutos();
	}
	
	@GetMapping("/{id}")
	public Optional<Produto> listarProdutoPorID(@PathVariable("id") int id) {
		return catalogoService.obterProdutoPorId(id);
	}
}
